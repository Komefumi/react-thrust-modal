import React from "react";

import Modal from "../Modal";
import classes from "./classes.module.styl";

const App = () => {
  return (
    <div className={classes.app}>
      <Modal title="Example Modal" />
    </div>
  );
};

export default App;
