import React from "react";
import clsx from "clsx";

import classes from "./classes.module.styl";

interface ModalProps {
  className?: string;
  title?: string;
}

const Dimmer = () => {
  return (
    <div className={classes.dimmer} />
  );
}

const Modal = ({ title, className }: ModalProps) => {
  const modalClassList = clsx(className, classes.modal);
  return (
    <div className={modalClassList}>
      <div className={classes.modalHeader}>
        <h2 className={classes.modalTitle}>{title}</h2>
      </div>
      <div>Modal is right here</div>
    </div>
  );
};

const FullModal = ({ className, title }: ModalProps) => {
  return (
    <div>
      <Modal className={className} title={title} />
      <Dimmer />
    </div>
  )
}

export default FullModal;
